﻿using System;
using NUnit.Framework;
using LogAn.Core;
using System;


namespace LogAn.UnitTests
{
    [TestFixture]
    public class LogAnalyzerTests
    {
        [Test]
        public void IsValidLogFileName_BadExtension_ReturnsFalse()
        {
            LogAnalyzer analyzer = new LogAnalyzer();
            bool result = analyzer.IsValidLogFileName("ft.foo");
            Assert.False(result);
        }
    }
}
